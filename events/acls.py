import json
import requests

from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    params = {"per_page": 1, "query": city + " " + state}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    return {"picture_url": content["photos"][0]["src"]["original"]}


def get_weather_data(city, state):
    geo_url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city}, {state}, US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    geo_response = requests.get(geo_url, params=params)
    geo_content = json.loads(geo_response.content)
    try:
        lat = geo_content[0]["lat"]
        lon = geo_content[0]["lon"]
    except (KeyError, IndexError):
        return None

    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    #print(content)
    try:
        return {
            "temp": content["main"]["temp"],
            "description": content["weather"][0]["description"],
        }
    except (KeyError, IndexError):
        return None
