from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Attendee
from events.models import Conference


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
    ]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }


def api_list_attendees(request, conference_id):
    attendees = Attendee.objects.all()
    return JsonResponse(
        {"attendees": attendees}, AttendeeListEncoder, safe=False
    )


def api_show_attendee(request, id):
    attendee = Attendee.objects.get(id=id)
    return JsonResponse(
        {
            "email": attendee.email,
            "name": attendee.name,
            "company_name": attendee.company_name,
            "created": attendee.created,
            "conference": {
                "name": attendee.name,
                "href": attendee.get_api_url(),
            },
        }
    )
